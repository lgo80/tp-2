#ifndef HEADER_H_INCLUDED
#define HEADER_H_INCLUDED

bool isExisteArchivo(); //si si
int obtenerFechaActual(); //si si
int obtenerEnteroAleatorio(int minimo, int maximo); //si si
bool agregarRegistroConductor(typeConductor conductor,char tipoLectura[], int cant); //si si
bool agregarRegistroInfraccion(typeInfraccion infraccion,char tipoLectura[], int cant); //si si
bool modificarRegistroConductor(long id, int cantidadRegistros); //si si
bool cargarNuevoConductor(); //si si
void crearDatosConductores(); //si si
void crearDatosInfracciones(long idConductor); //si si
void mostrarConductor(typeConductor conductor); //si si
int mostrarTodoConductores(void); //no
bool levantarConductores(); //si si
int mostrarTodoInfracciones(void); //no
void desplegarMenu(); //si si
void limpiarPantalla(bool isError, char mensaje[]); //si si
void mostrarInfraccionConductor(typeConductor conductor); //si
void mostrarResultado(char valor[], bool withSeparation); //si si
int buscar(char email[], char opt); //si si
bool validar(char valor[], char lugar); //si si
char opcionElegida(); //si si
int buscarConductor(char opt); //si si
void buscarConductorConjunto(char &opt); //si si
void finalizarJornada(); //si si
int buscarInfracciones(long conductorID, int opcionInt,float &sumaMonto, bool isProvincia);//si si
int infraccionesPorProvincia(); //no
int actualizarTotalInfracciones(); //si si
int procesarLoteInfracciones(); //si si
long calcularNumeroRegistros(); //si si
char getOpcion(char opcion); //si si
char menu(); //si si
char reiniciarOpciones(bool isPrimeraVez); //si si

#endif
