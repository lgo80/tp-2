#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <conio.h>
#include <sstream>
#include <ctime>
#include <io.h>
#include <wchar.h>
#include <locale.h>
#define FILE_CONDUCTORES "Conductores.bin"
#define FILE_PROCESADOS "Procesados.bin"
#define FILE_INFRACCIONES "Infracciones.bin"
#define VACIO_STRUCT -1000
#define ESC 27
#define VALOR_S 's'
#define VALIDAD_ID 'i'
#define VALIDAD_VENC 'v'
#define VALIDAD_EMAIL 'e'
#define BUSCAR_B 'b'
struct Conductor
{
    long conductorID;
    int fechaVencimiento;
    int totalInfracciones;
    char eMail[40];
    bool activo = true;
};
struct Infraccion
{
    long infraccionID;
    char fechaHora[15];
    float monto;
    long conductorID;
    int provincia;
};
typedef Conductor typeConductor;
typedef Infraccion typeInfraccion;
#include "header.h"

typeConductor *conductores;
int cantidadConductores;
