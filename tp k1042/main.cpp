#include "initials.cpp"
using namespace std;

bool isExisteArchivo()
{
    FILE *arch;
    arch=fopen("Conductores.bin","rb");
    return (arch!=NULL);
}

int obtenerFechaActual()
{

    time_t t = time(NULL);
    stringstream ss_year,ss_month,ss_day;
    tm* timePtr = localtime(&t);

    ss_year << timePtr->tm_year+1900;
    ss_month << timePtr->tm_mon+1;
    ss_day << timePtr->tm_mday;

    string year = ss_year.str();
    string month = ss_month.str();
    string day = ss_day.str();

    return atoi(year.c_str()) * 10000 + atoi(month.c_str()) * 100 + atoi(day.c_str());
}

int obtenerEnteroAleatorio(int minimo, int maximo)
{
    return rand() % (maximo-minimo+1)+minimo;
}

bool validar(char valor[], char lugar)
{
    int valorInt;
    switch (lugar)
    {
    case VALIDAD_ID:

        valorInt = atoi(valor);
        if(valorInt <= 0)
        {
            return false;
        }
        return !buscar(valor, BUSCAR_B);
    case VALIDAD_VENC:

        valorInt = atoi(valor);
        return (valorInt >= 20000000 && valorInt <= 20400000);
    case VALIDAD_EMAIL:
        return !buscar(valor, BUSCAR_B);
    }
    return 1;
}

bool agregarRegistroConductor(typeConductor conductor,char tipoLectura[], int cant)
{

    bool isValido = false;
    FILE *f;
    if (f=fopen(FILE_CONDUCTORES,tipoLectura))
    {
        fwrite(&conductor, sizeof(typeConductor),cant,f);

        isValido = true;
    }
    fclose(f);
    return isValido;
}

bool agregarRegistroInfraccion(typeInfraccion infraccion,char tipoLectura[], int cant)
{

    bool isValido = false;
    FILE *f;
    if (f=fopen(FILE_INFRACCIONES,tipoLectura))
    {
        fwrite(&infraccion, sizeof(typeInfraccion),cant,f);

        isValido = true;
    }
    fclose(f);
    return isValido;
}

bool modificarRegistroConductor(long id, int cantidadRegistros)
{

    bool isValido = false;
    FILE *f;
    typeConductor conductor;
    if (f=fopen(FILE_CONDUCTORES,"rb+"))
    {

        while(fread(&conductor,sizeof(typeConductor),1,f))
        {
            if (conductor.conductorID == id)
            {
                isValido = true;
                if(cantidadRegistros == VACIO_STRUCT)
                    conductor.activo = false;
                else
                    conductor.totalInfracciones = cantidadRegistros;
                fseek(f,(-1)*sizeof(typeConductor), SEEK_CUR);
                fwrite(&conductor,sizeof(typeConductor),1,f);
                break;
            }
        }

    }
    fclose(f);
    return isValido;
}

bool cargarNuevoConductor()
{
    typeConductor conductor;
    char lectura[5],valor[30];
    do
    {
        mostrarResultado("Ingrese el id del conductor:", false);
        cin >>  valor;
    }
    while (!validar(valor,VALIDAD_ID));
    conductor.conductorID = atoi(valor);
    do
    {
        mostrarResultado("Ingrese la fecha de vencimiento (AAAAMMDD) del conductor:", false);
        cin >>  valor;
    }
    while (!validar(valor,VALIDAD_VENC));
    conductor.fechaVencimiento = atoi(valor);
    do
    {
        mostrarResultado("Ingrese el email del conductor:", false);
        cin >>  valor;
    }
    while (!validar(valor,VALIDAD_EMAIL));
    strcpy(conductor.eMail, valor);
    conductor.totalInfracciones = 0;
    conductor.activo = (conductor.fechaVencimiento >= obtenerFechaActual());
    strcpy(lectura,"ab");
    if(agregarRegistroConductor(conductor,lectura,1))
    {
        levantarConductores();
        return true;
    }

    return false;
}

void crearDatosInfracciones(long idConductor)
{

    typeInfraccion infraccion;
    int isHaveFines, countFines = 0, i;
    bool isExito;
    char lectura[5], valor[15];
    int fecha, hora,segundo;
    long result;

    isHaveFines = obtenerEnteroAleatorio(0,1);
    if(isHaveFines)
    {
        countFines = obtenerEnteroAleatorio(1,20);
        for(i = 0; i < countFines; i++)
        {
            infraccion.infraccionID = obtenerEnteroAleatorio(1000,10000);
            fecha = (obtenerEnteroAleatorio(2019,2040) * 10000) + (obtenerEnteroAleatorio(1,12) * 100) + (obtenerEnteroAleatorio(1,28));
            hora = obtenerEnteroAleatorio(0,24);
            segundo = obtenerEnteroAleatorio(0,59);
            sprintf(valor, "%d %d:%d", fecha,hora,segundo);
            strcpy(infraccion.fechaHora,valor);
            infraccion.monto = obtenerEnteroAleatorio(1000,20000);
            infraccion.conductorID = idConductor;
            infraccion.provincia = obtenerEnteroAleatorio(1,24);

            strcpy(lectura,"ab");
            isExito = agregarRegistroInfraccion(infraccion,lectura,1);
        }

    }
    return;
}

void crearDatosConductores()
{
    typeConductor conductor[30];
    for(int i = 0; i < 30; i++)
    {
        int fechaVenc = (obtenerEnteroAleatorio(2019,2040) * 10000) + (obtenerEnteroAleatorio(1,12) * 100) + (obtenerEnteroAleatorio(1,28));
        conductor[i].conductorID = i + 1;
        conductor[i].fechaVencimiento = fechaVenc;
        sprintf(conductor[i].eMail, "email%d@yahoo.com.ar", i + 1);
        conductor[i].totalInfracciones = 0;
        conductor[i].activo = (conductor[i].fechaVencimiento >= obtenerFechaActual());
    }

    FILE *f;
    if (f=fopen(FILE_CONDUCTORES,"wb"))
    {
        fwrite(conductor, sizeof(typeConductor),30,f);
        fclose(f);
    }
    return;
}

void mostrarConductor(typeConductor conductor)
{
    mostrarResultado("****************************Datos****************************", false);
    char valor[50];
    sprintf(valor, "Id: %d", conductor.conductorID);
    mostrarResultado(valor, false);
    sprintf(valor, "fechaVencimiento: %d", conductor.fechaVencimiento);
    mostrarResultado(valor, false);
    sprintf(valor, "eMail: %s", conductor.eMail);
    mostrarResultado(valor, false);
    sprintf(valor, "totalInfracciones: %d", conductor.totalInfracciones);
    mostrarResultado(valor, false);
    sprintf(valor, "activo: %d", conductor.activo);
    mostrarResultado(valor, false);
}

int mostrarTodoConductores(void)
{
    FILE *f;
    typeConductor conductor;
    if (f=fopen(FILE_CONDUCTORES,"rb"))
    {
        fread(&conductor,sizeof(typeConductor),1,f);
        while(!feof(f))
        {
            mostrarConductor(conductor);
            fread(&conductor,sizeof(typeConductor),1,f);
        }
        fclose(f);
        return 1;
    }
    return 0;
}

int mostrarTodoInfracciones(void)
{
    FILE *f;
    typeInfraccion infraccion;
    if (f=fopen(FILE_INFRACCIONES,"rb"))
    {
        fread(&infraccion,sizeof(typeInfraccion),1,f);
        while(!feof(f))
        {
            mostrarResultado("****************************Datos****************************", false);
            char valor[50];
            sprintf(valor, "Id: %d", infraccion.infraccionID);
            mostrarResultado(valor, false);
            sprintf(valor, "Fecha y hora: %s", infraccion.fechaHora);
            mostrarResultado(valor, false);
            sprintf(valor, "Monto: %.2f", infraccion.monto);
            mostrarResultado(valor, false);
            sprintf(valor, "Id conductor: %d", infraccion.conductorID);
            mostrarResultado(valor, false);
            sprintf(valor, "idProvincia: %d", infraccion.provincia);
            mostrarResultado(valor, false);
            fread(&infraccion,sizeof(typeInfraccion),1,f);
        }
        fclose(f);
        return 1;
    }
    return 0;
}

void desplegarMenu()
{
    mostrarResultado("Elija la opcion deseada:", false);
    mostrarResultado("1 - Comenzar jornada", false);
    mostrarResultado("2 - Cargar nuevo conductor", false);
    mostrarResultado("3 - Desactivar un conductor existente", false);
    mostrarResultado("4 - Buscar un Conductor por ID o por mail", false);
    mostrarResultado("5 - Lista completa de infracciones de un conductor", false);
    mostrarResultado("6 - Listar conductores con infraciones por provincia", false);
    mostrarResultado("7 - Procesar un lote de infracciones", false);
    mostrarResultado("8 - Finalizar jornada", false);
    mostrarResultado("Esc - Salir", false);
}

void limpiarPantalla(bool isError, char mensaje[])
{
    if(isError)
    {
        system("cls");
        if(mensaje != "")
            mostrarResultado(mensaje, false);
    }
}

void mostrarInfraccionConductor(typeConductor conductor)
{
    FILE *f;
    typeInfraccion infraccion;
    float sumaMonto = 0;
    char name[50];
    if (f=fopen(FILE_PROCESADOS,"rb"))
    {
        while(fread(&infraccion,sizeof(typeInfraccion),1,f))
        {

            if(conductor.conductorID == infraccion.conductorID )
                sumaMonto += infraccion.monto;

        }
    }

    sprintf(name, "La cantidad de infracciones del conductor es: %d y el monto total se eleva a: %.2f", conductor.totalInfracciones,sumaMonto);
    mostrarResultado(name,true);
}

void mostrarResultado(char valor[], bool withSeparation)
{
    if(withSeparation)
        cout << "******************************************************************" << endl;
    cout << valor << endl;
    if(withSeparation)
        cout << "******************************************************************" << endl;
}
int buscar(char email[], char opt)
{

    int i = 0,valor,encontrado = 0;
    char out;
    while(i < cantidadConductores && encontrado == 0)
    {
        valor = atoi(email);
        if (valor == conductores[i].conductorID || strcmp(email, conductores[i].eMail)==0)
        {
            switch(opt)
            {
            case '3':
                conductores[i].activo = 0;
                modificarRegistroConductor(conductores[i].conductorID, VACIO_STRUCT);
                mostrarResultado("El conductor ingresado fue desactivado correctamente", true);
                break;
            case '4':
                mostrarConductor(conductores[i]);
                break;
            case '5':
                mostrarInfraccionConductor(conductores[i]);
                break;
            case BUSCAR_B:
                return 1;
            }
            encontrado = 1;
        }
        i++;
    }
    if (encontrado == 0)
    {
        if (opt != BUSCAR_B)
        {
            mostrarResultado("no se encontro el conductor solicitado", true);
            mostrarResultado("Presione cualquier tecla si quiere volver a buscar otro conductor o esc para volver al menu principal", false);
            out = getch();
            return (out == ESC) ? ESC : 0;
        }
        else
            return 0;
    }
    return 1;
}

char opcionElegida()
{
    char opcion;
    bool isError = false;
    do
    {
        limpiarPantalla(isError,"El dato ingresado no es correcto, por favor ingrese un dato correcto\n");
        isError = true;
        desplegarMenu();
        opcion = getch();
    }
    while (!(opcion >= '0' && opcion <= '9') && (opcion != ESC));
    return opcion;
}

int buscarConductor(char opt)
{
    char valor[10];
    mostrarResultado("Ingrese el id o email del conductor:",false);
    cin >>  valor;
    return buscar(valor,opt);
}

void buscarConductorConjunto(char &opt)
{
    int  isEncontro;
    do
    {
        isEncontro = buscarConductor(opt);
    }
    while(isEncontro == 0);
    if(isEncontro == ESC)
    {
        limpiarPantalla(true,"");
        opt = VALOR_S;
    }
}

void finalizarJornada()
{

    FILE *f;
    f = fopen(FILE_CONDUCTORES,"wb");
    for(int i = 0; i < cantidadConductores; i++)
    {
        if (conductores[i].activo && conductores[i].fechaVencimiento >= obtenerFechaActual())
        {
            fwrite(&conductores[i], sizeof(typeConductor),1,f);
        }
    }
    fclose(f);
    mostrarResultado("Finalizo jornada", true);
}

int buscarInfracciones(long conductorID, int opcionInt,float &sumaMonto, bool isProvincia)
{
    FILE *fi;
    int encontrado =0;
    int cantInfracciones = 0;
    typeInfraccion infraccion;
    if (fi=fopen(FILE_PROCESADOS,"rb"))
    {
        fread(&infraccion,sizeof(typeInfraccion),1,fi);
        while(!feof(fi))
        {
            if ((conductorID == infraccion.conductorID && isProvincia && opcionInt == infraccion.provincia)
                    || (conductorID == infraccion.conductorID && !isProvincia))
            {

                sumaMonto += infraccion.monto;
                cantInfracciones++;
            }
            fread(&infraccion,sizeof(typeInfraccion),1,fi);
        }

    }
    fclose(fi);
    return cantInfracciones;
}

int infraccionesPorProvincia()
{

    char opcion[2];
    int opcionInt=0;
    do
    {
        mostrarResultado("Elija la provincia deseada:",false);
        mostrarResultado("1- Buenos Aires\t\t2- Capital Federal\t\t3- Catamarca\t\t4- Chaco",false);
        mostrarResultado("5- Chubut\t\t6- C�rdoba\t\t\t7- Corrientes\t\t8- Entre R�os",false);
        mostrarResultado("9- Formosa\t\t10- Jujuy\t\t\t11- La Pampa\t\t12- La Rioja",false);
        mostrarResultado("13- Mendoza\t\t14- Misiones\t\t\t15- Neuqu�n\t\t16- R�o Negro",false);
        mostrarResultado("17- Salta\t\t18- San Juan\t\t\t19- San Luis\t\t20- Santa Cruz",false);
        mostrarResultado("21- Santa Fe\t\t22- Santiago del Estero\t\t23- Tierra del Fuego\t24- Tucum�n",false);
        mostrarResultado("99 - Volver al menu",false);
        cin >> opcion;
        opcionInt = atoi(opcion);
    }
    while (!(opcionInt > 0 && opcionInt < 25) && (opcionInt != 99));

    if(opcionInt == 99)
    {
        limpiarPantalla(true,"");
        return VALOR_S;
    }

    float sumaMonto = 0;
    char valor[50];
    int cantInfracciones = 0, isEntro = 0;
    for(int i = 0; i < cantidadConductores; i++)
    {
        cantInfracciones = buscarInfracciones(conductores[i].conductorID,opcionInt,sumaMonto,true);
        if (cantInfracciones > 0)
        {
            mostrarResultado("************************************************************", false);
            mostrarResultado("Los conductores con infracciones en la provincia elegida son:", false);
            sprintf(valor, "Id: %d", conductores[i].conductorID);
            mostrarResultado(valor, false);
            sprintf(valor, "fechaVencimiento: %d", conductores[i].fechaVencimiento);
            mostrarResultado(valor, false);
            sprintf(valor, "eMail: %s", conductores[i].eMail);
            mostrarResultado(valor, false);
            sprintf(valor, "activo: %d", conductores[i].activo);
            mostrarResultado(valor, false);
            sprintf(valor, "El monto total de infracciones en la provincia: %.2f", sumaMonto);
            mostrarResultado(valor, false);
            sprintf(valor, "La cantidad total de infracciones en la provincia: %d", cantInfracciones);
            mostrarResultado(valor, false);
            isEntro = 1;
        }
    }

    if(!isEntro)
        mostrarResultado("No hay infracciones en la provincia ingresada", true);
    return 0;
}

int actualizarTotalInfracciones()
{

    int cantInfracciones, isValido = 0;
    float sumaMonto = 0;
    for(int i = 0; i < cantidadConductores; i++)
    {

        cantInfracciones = buscarInfracciones(conductores[i].conductorID,0,sumaMonto,false);
        conductores[i].totalInfracciones = cantInfracciones;
        modificarRegistroConductor(conductores[i].conductorID, cantInfracciones);
        isValido = 1;
    }

    return isValido;

}

int procesarLoteInfracciones()
{

    int i = 0;
    FILE *fi,*fp;
    typeInfraccion infraccion;

    for(int i = 0; i < cantidadConductores; i++)
    {
        crearDatosInfracciones(conductores[i].conductorID);
    }

    fp=fopen(FILE_PROCESADOS,"ab");
    if (fi=fopen(FILE_INFRACCIONES,"rb"))
    {
        fread(&infraccion,sizeof(typeInfraccion),1,fi);
        while(!feof(fi))
        {

            fwrite(&infraccion, sizeof(typeInfraccion),1,fp);
            fread(&infraccion,sizeof(typeInfraccion),1,fi);
        }
    }

    fclose(fi);
    fclose(fp);

    return actualizarTotalInfracciones();

}

long calcularNumeroRegistros()
{
    FILE *pFichero;
    long numeroRegistros;

    pFichero = fopen("Conductores.bin", "rb");
    fseek(pFichero, 0, SEEK_END);
    numeroRegistros = ftell(pFichero)/sizeof(typeConductor);

    fclose(pFichero);
    return numeroRegistros;
}

bool levantarConductores()
{
    bool isExiste = isExisteArchivo(), isValido = false;
    FILE *f;
    typeConductor conductor;
    int i = 0;
    if(!isExiste)
    {
        crearDatosConductores();
    }
    cantidadConductores = calcularNumeroRegistros();
    conductores = new typeConductor[cantidadConductores];

    if (f=fopen(FILE_CONDUCTORES,"rb"))
    {
        fread(&conductor,sizeof(typeConductor),1,f);
        while(!feof(f))
        {
            conductores[i].conductorID = conductor.conductorID;
            conductores[i].fechaVencimiento = conductor.fechaVencimiento;
            sprintf(conductores[i].eMail, conductor.eMail);
            conductores[i].totalInfracciones = conductor.totalInfracciones;
            conductores[i].activo = conductor.activo;
            i++;
            fread(&conductor,sizeof(typeConductor),1,f);
        }
        isValido = true;
    }
    fclose(f);
    return isValido;
}

char getOpcion(char opcion)
{
    int i2;
    bool isExito;
    switch (opcion)
    {
    case '1':

        isExito = levantarConductores();
        if(isExito)
            mostrarResultado("Los datos de los conductores se levantaron correctamente", true);
        break;
    case '2':
        isExito = cargarNuevoConductor();
        if(isExito)
            mostrarResultado("El conductor ingresado se creo exitosamente",true);
        break;
    case '3':
        buscarConductorConjunto(opcion);
        break;
    case '4':
        buscarConductorConjunto(opcion);
        break;
    case '5':
        buscarConductorConjunto(opcion);
        break;
    case '6':
        opcion = infraccionesPorProvincia();
        break;
    case '7':
        isExito = procesarLoteInfracciones();
        if(isExito)
            mostrarResultado("El lote se proceso correctamente",true);
        break;
    case '8':
        finalizarJornada();
        break;
    case '9':
        i2 = mostrarTodoInfracciones();

        break;
    case '0':
        i2 = mostrarTodoConductores();
        break;
    }

    return opcion;
}

char menu()
{
    char opcion = opcionElegida();
    return getOpcion(opcion);
}

char reiniciarOpciones(bool isPrimeraVez)
{
    if(!isPrimeraVez)
    {
        mostrarResultado("Ingrese una tecla para volver al menu principal o la tecla esc para salir del programa",false);
        char out = getch();
        if (out == ESC)
            return out;
        system("cls");
    }
    return menu();
}

int main()
{

    setlocale(LC_ALL, "");
    char out;
    bool isPrimeraVez = true;
    do
    {
        out = reiniciarOpciones(isPrimeraVez);
        isPrimeraVez = (out == VALOR_S);
    }
    while (out != ESC);

    delete [] conductores;

    return 0;
}
